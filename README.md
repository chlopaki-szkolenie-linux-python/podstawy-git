Podstawy git

# Podstawowe komendy

`git add` - dodaje pliki

`git commit` - tworzy komit z flaga `-m` razem z opisem

`git pull` - pobiera zmiany z repozytorium

`git push` - wrzuca zmiany na repozytorium
